---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Jianjun Feng. I have the following qualities:

- I love programming in Java and Python, among other languages.
- I write code in Java at day and Python at night.
- I love the combination of math, statistics and programming, which nowadays is called Data Science/Machine Learning/Deep Learning/Artificial Intelligence.



### my history

To be honest, I'm having some trouble remembering right now.

