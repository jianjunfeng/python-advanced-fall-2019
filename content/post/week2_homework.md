---
title: Week2 Homework 
subtitle: All about loops 
date: 2019-09-10 
tags: ["loop","function"]
draft: false 
---


This week's homework is all about loops.
<!--more-->

Last week, we wrote a function to print the sequence of numbers:

**0,2,4,6,8,....M** (*M being the max in the sequence.*)

The code was like this:
```
def print_even(M):
    M = M//2+1
    for i in range(M):
        print(i*2, end=',')

print_even(20)
print("\n")
print_even(30) 
print("\n")

```

### The homework assignments for this week: ###

1) Write a function to print the following sequence of numbers:

**1,3,5,7,9,...M**

```
def print_odd(M):
	#write your code to replace pass
	pass

print_odd(20)
print("\n")
print_odd(30) 
print("\n")

```



2) Study the sequence of numbers:

**0, 1, 4, 9, 16, 25, 36,...**

Write a function to print the first n number in the sequence.
For example, the first 5 numbers are:
0,1,4,9,16

To get you started, I have the following Python code you can use:
```
def print_sq(n):
    #write your code to replace pass
    pass


n=int(input("Enter a number between 2 and 100:"))
print_sq(n)

```
