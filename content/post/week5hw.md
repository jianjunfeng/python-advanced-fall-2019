---
title: Week5 Homework 
subtitle: Book Mission Python Chapter 1 
date: 2019-10-04
tags: ["pygame zero","python","mu"]
draft: false 
---


Please finish Chapter 1 of Mission Python.
<!--more-->

If you don't have the book, your code should look like this:

```python

WIDTH = 800
HEIGHT = 600
player_x=600
player_y=350

def draw():
    screen.blit(images.backdrop, (0,0))
    screen.blit(images.mars, (50,50))
    screen.blit(images.astronaut, (player_x,player_y))
    screen.blit(images.ship, (130,150))

def update():
    global player_x, player_y
    if keyboard.right:
        player_x += 5
    elif keyboard.left:
        player_x -= 5
#finish the rest of the code by adding up and down controls

```

**Bonus points:**

Use WASD instead of arrow keys as control.


