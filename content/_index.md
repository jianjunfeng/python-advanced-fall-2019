## Announcement from Teacher Jianjun Feng

Welcome to the Python for Kids(Advanced) class!

Most of you should have taken my Python for Kids(Beginner) class last semester. If you haven't and want to get started with Python, please consider taking the Biginner class first.

You'll find **homework assignments** on this webiste. I will stop emailing assignments. I'll use email for important announcements and private issues.

