---
title: Week9 Homework 
subtitle: Loop and lists 
date: 2019-11-09
tags: ["pygame zero","python","mu"]
draft: false 
---

The code example and related images were created by Rik Cross.

Read the code and answer the two questions about function update().

```Python
        x.left -= s # Why do we do this? 
        s += 1   # what does this mean?
```

The full source code is here:

```Python

WIDTH = 800
HEIGHT = 400

layer_back = Actor('image_back')
layer_back.topleft = 0, 0

layer_middle = Actor('image_middle')
layer_middle.topleft = 0, 0

layer_front = Actor('image_front')
layer_front.topleft = 0, 0

layers = [layer_back,layer_middle,layer_front]

def draw():
    screen.clear()
    for x in layers:
        x.draw()

def update():
    s = 1 # read the code and trace variable s. 
    for x in layers:
        print(s)
        x.left -= s # Why do we do this? 
        s += 1   # what does this mean?
        if x.right < WIDTH:
            x.left = 0

```

