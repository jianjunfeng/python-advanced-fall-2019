---
title: Week3 Homework 
subtitle: Review on Turtle 
date: 2019-09-17
tags: ["loop","turtle","math"]
draft: false 
---


This week's homework is a review of what we've learned about turtle last semester.
<!--more-->

I created a turtle program with one flaw:

The turtle moves forward even after it runs out of the visible window.

I'd you to copy the code into Mu-editor; run it and see the flaw; modify the code so that turtle will make a turn before it goes outside of the window.

```
import turtle
import random

t=turtle.Turtle()
t.left(random.randint(0,360))
for i in range(200):
    t.fd(10)
    x,y=t.position()
    print(x,y)
    #add your code here
    
print('Done!')

```

