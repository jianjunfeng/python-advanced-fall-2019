---
title: Week1 Homework 
subtitle: How much water is left? 
date: 2019-08-26 
tags: []
draft: false 
---


This week's homework is based on a Python programming competition task.
<!--more-->

[The original task is here](https://atcoder.jp/contests/abc136/tasks/abc136_a).  I'll repeat the task here and provide a starter code.

We have two bottles for holding water.
Bottle 1 can hold up to A milliliters of water, and now it contains B milliliters of water.

Bottle 2 contains C milliliters of water.  We will transfer water from Bottle 2 to Bottle 1 as much as possible.

How much amount of water will remain in Bottle 2?

For example, if A is 16, B is 12, C is 8, then the answer will be 4.

To get you started, I have the following Python code you can use:
```
def water_left(A,B,C):
    #obviously return 0 is wrong. you need to fix it.
    return 0 

A=int(input("Enter value of A:"))
B=int(input("Enter value of B:"))
C=int(input("Enter value of C:"))
D=water_left(A,B,C)
print(D)
```
