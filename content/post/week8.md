---
title: Week8 Homework 
subtitle: Book Mission Python Chapter 4 
date: 2019-10-31
tags: ["pygame zero","python","mu"]
draft: false 
---

Please finish the first section of Chapter 4 (page 60~68) 
<!--more-->

Chapter 4 is very challenging. We'll slow down our pace.

To help you understand the code in the book in Chapter 4, I created the following code.

It's similiar to the the code in the book, but it creates two rooms instead of 50.

Copy the code into MU editor and play with it using Up and Down keys. 

Do you understand the code?  If not, write down your questions and we'll discuss it next week.


```Python

###simplified  code from Chapter 4
### we draw two rooms instead of 50!

WIDTH = 800 #window size
HEIGHT = 800

current_room = 0

top_left_x = 100
top_left_y = 150

DEMO_OBJECTS = [images.floor, images.pillar, images.soil]

MAP_WIDTH = 5
MAP_HEIGHT = 10
MAP_SIZE = MAP_WIDTH * MAP_HEIGHT

room1=[[1,1,1,1,1,1,1,1,1],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,0],
       [1,0,0,2,2,2,0,0,0],
       [1,0,0,2,2,2,0,0,0],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,1],
       [1,1,1,0,0,0,1,1,1]
    ]
room2=[[1,1,1,0,0,0,1,1,1],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,0],
       [1,0,0,0,0,0,0,0,0],
       [1,0,0,0,1,0,0,0,0],
       [1,0,0,0,0,0,0,0,1],
       [1,0,0,0,0,0,0,0,1],
       [1,1,1,1,1,1,1,1,1]
    ]

GAME_MAP = [ room1, room2]


###############
## EXPLORER  ##
###############

def draw():
    room_map=GAME_MAP[current_room]
    room_height=len(room_map)
    room_width=len(room_map[0]) 
    screen.clear()

    for y in range(room_height):
        for x in range(room_width):
            image_to_draw = DEMO_OBJECTS[room_map[y][x]]
            screen.blit(image_to_draw,
                (top_left_x + (x*30),
                 top_left_y + (y*30) - image_to_draw.get_height()))

def on_key_down(key):
    global current_room
    old_room = current_room

    if key == keys.LEFT:
        current_room -= 1
    if key == keys.RIGHT:
        current_room += 1
    if key == keys.UP:
        current_room -= MAP_WIDTH
    if key == keys.DOWN:
        current_room += MAP_WIDTH

    if current_room > 1:
        current_room = 1
    if current_room < 0:
        current_room = 0

    if current_room != old_room:
        print("Entering room:" + str(current_room))


```



