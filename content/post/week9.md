---
title: Week9 Images for coding excersie 
subtitle: Rick Ross created the images 
date: 2019-11-03
tags: ["pygame zero","python","mu"]
draft: false 
---

We'll use the three images for coding excercise.
(The code example and related images were created by Rik Cross.)

-- Back
![Layer_back](/image_back.png)

-- Middle
![Layer_middle](/image_middle.png)

-- Front
![Layer_front](/image_front.png)

Download them and save them in mu_code/images. 