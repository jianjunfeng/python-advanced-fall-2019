---
title: Week11 Homework 
subtitle: Inspired by the snake game 
date: 2019-11-14
tags: ["pygame zero","python","mu"]
draft: false 
---

Last week we started writing a game. Each student has its own version of the game.

We haven't finished it yet. 

Here's my version of the game.  

Pay attention to the function update().
There're a few problems with code. Can you see the problems and fix them?
Could you add the update() function to your game?

```Python

import random
WIDTH = 800
HEIGHT = 800

speed = 4
head = Actor('spaceship',pos=(400,400))

x=random.randint(0,700)
y=random.randint(0,700)
target = Actor('credit',pos=(x,y))

tails = []
x=400
y=400
for i in range(3):
    x=x - 35
    tails.append(Actor('player',pos=(x,y)))

def draw():
    head.draw()
    target.draw()
    for t in tails:
        t.draw()

def update():
    if keyboard.up:
        head.angle=90
        head.y -= speed
    if keyboard.down:
        head.angle=270
        head.y += speed
    if keyboard.left:
        head.angle=180
        head.y -= speed
    if keyboard.right:
        head.angle=0
        head.y += speed



```






-- Head
![head](/spaceship.png)

-- Tail
![tail](/player.png)

-- Target
![target](/credit.png)

Download them and save them in mu_code/images. 