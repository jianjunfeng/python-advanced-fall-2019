---
title: Week7 Homework 
subtitle: Book Mission Python Chapter 3 
date: 2019-10-24
tags: ["pygame zero","python","mu"]
draft: false 
---

Please finish Chapter 3 of Mission Python.
<!--more-->

This is what we have after Chapter 3.  The code is not a direct copy from the book.  I modified the code to help you understand it better.

**Your assignments are:**

1. Change the 5x7 room_map to a 9x9 room_map.
2. Use at least 4 images in DEMO_OBJECTS list
3. Use the images in DEMO_OBJECTS in your room_map.

```Python

room_map = [ [ 1, 1, 1, 1, 1],
             [ 1, 0, 0, 0, 1],
             [ 1, 0, 2, 0, 1],
             [ 1, 0, 0, 0, 1],
             [ 1, 0, 0, 0, 1],
             [ 1, 0, 0, 0, 1],
             [ 1, 1, 1, 1, 1]
           ]

WIDTH = 800 #window size
HEIGHT = 800

top_left_x = 100
top_left_y = 150

DEMO_OBJECTS = [images.floor, images.pillar, images.cactus]

room_height = 1
room_width = 1

def draw():
    screen.clear()
    for y in range(room_height):
        for x in range(room_width):
            image_to_draw = DEMO_OBJECTS[room_map[y][x]]
            screen.blit(image_to_draw,
                (top_left_x + (x*30),
                top_left_y + (y*30) - image_to_draw.get_height()))

def on_key_down(key):
    global room_height, room_width
    if key == keys.DOWN:
        if room_height < 7:
            room_height += 1
    elif key == keys.RIGHT:
        if room_width < 5:
            room_width += 1
    elif key == keys.UP:
        if room_height > 1:
            room_height -= 1
    elif key == keys.LEFT:
        if room_width > 1 :
            room_width -= 1

```
